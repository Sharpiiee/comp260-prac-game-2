﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {
	private Rigidbody rigidbody;

	float moveSpeed = 4f;
	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
		rigidbody.useGravity = false;

	}
	
	// Update is called once per frame
	void Update () {
		float moveVertical = Input.GetAxis ("Vertical");
		Vector3 movement = new Vector3(Input.GetAxis("Horizontal"),(moveVertical),0.0f);

		rigidbody.velocity = movement * moveSpeed;
	
	}
	private Vector3 GetMousePosition () {
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

		Plane plane = new Plane (Vector3.forward, Vector3.zero);
		float distance = 0;
		plane.Raycast (ray, out distance);
		return ray.GetPoint (distance);
	}

	void OnDrawGizmos() {
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition ();
		Gizmos.DrawLine (Camera.main.transform.position, pos);
	}

	public float speed = 20f;
	public float force = 10f;

	/* void FixedUpdate () {
		Vector3 pos = GetMousePosition ();
		Vector3 dir = pos - rigidbody.position;
		rigidbody.AddForce (dir.normalized * force);
		Vector3 vel = dir.normalized * speed;

		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;

		if (move > distToTarget) {
			vel = vel * distToTarget / move;
		}

		rigidbody.velocity = vel;


	}
*/
}