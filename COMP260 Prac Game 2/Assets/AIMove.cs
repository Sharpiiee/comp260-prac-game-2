﻿using UnityEngine;
using System.Collections;

public class AIMove : MonoBehaviour {
	float maxValue = 3.0f;
	float minValue = -3.0f;
	float currentValue = 1.0f;
	float direction =5.0f;


	void Start(){
		
	}

	void Update(){
		currentValue += Time.deltaTime * direction;
		if (currentValue >= maxValue) {
			direction *= -1;
			currentValue = maxValue;
		} else if (currentValue <= minValue) {
			direction *= -1;
			currentValue = minValue;
		}
		transform.position = new Vector3 (5, currentValue, 0);
	}

}