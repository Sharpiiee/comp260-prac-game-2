﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (AudioSource))]
public class PuckControl : MonoBehaviour {

	public AudioClip wallCollideClip;
	public AudioClip paddleCollideClip;
	public LayerMask paddleLayer;
	private AudioSource audio;
	public Transform startingPos;
	private Rigidbody rigidbody;



	void OnCollisionEnter (Collision collision) {
		Debug.Log ("Control Enter: " + collision.gameObject.name);


		if (paddleLayer.Contains (collision.gameObject)) {
			audio.PlayOneShot (paddleCollideClip);
		} else {
			audio.PlayOneShot (wallCollideClip);
		}
	}

	void OnCollisionStay (Collision collision) {
		Debug.Log ("Control Stay: " + collision.gameObject.name);
	}

	void OnCollisionExit (Collision collision) {
		Debug.Log ("Control Exit: " + collision.gameObject.name);
	}




	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource>();
		rigidbody = GetComponent<Rigidbody>();
		ResetPosition();

	
	}

	public void ResetPosition () {
		rigidbody.MovePosition(startingPos.position);
		rigidbody.velocity = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () {
		
	
	}
}
